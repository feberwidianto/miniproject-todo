import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import TodoList from './components/TodoList';
import Login from './components/Login';
import SignUp from './components/SignUp';
import { Navbar, NavbarBrand, Nav, NavbarToggler, NavItem, Collapse } from 'reactstrap';
import AddTodo from './components/AddTodo';
import ItemTodo from './components/ItemTodo';


class App extends Component {
  constructor(props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: true,
      islog: true,
      todos: [
        {
          activity: 'Menulis',
          duration: 12,
          time: 12,
          priority: false
        },
        {
          activity: 'Membaca',
          duration: 12,
          time: 12,
          priority: false
        },
        {
          activity: 'Belajar',
          duration: 12,
          time: 12,
          priority: false
        }
      ]
    };
  }

  toggleNavbar() {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  // deleteTodo = (index) => {
  //   let array = [...this.state.todos]
  //   array.splice(index,1)
  //   this.setState({
  //     todos: array
  //   })
  // }
  // checkList = () => {

  // }

  islog = e => this.setState({
    islog: false
  })

componentDidMount(){
  if (localStorage.getItem('token') !== null){
    this.setState({ tokenValue: localStorage.getItem('token')})
    this.setState({ token: true})
  }
}

//GET API
// axios.get('', {
//   header: {'Authorization': localStorage.getItem('token')}
// })
// .then(res => this.setState({
//   todos : 
// }))


 
//ADD TODO/CREATE/POST
AddTodo = (activity, duration, time, priority) => {
  const newTodo = { activity, duration, time, priority }
  this.setState({
    todos: [...this.state.todos, newTodo]
  })
}
//PRINT TODO/GET/READ
//CHECKLIST TODO/PUT/UPDATE
//DELETE TODO/DELETE/DELETE




  render() {
    console.log(this.state.todos)
    return (
      <Router>
        <div className="home">
          <Navbar color="faded" light>
            <NavbarBrand href="/" className="mr-auto">NELF TODOAPP</NavbarBrand>
            <NavbarToggler onClick={this.toggleNavbar} className="ml-2" />
            <Collapse isOpen={!this.state.collapsed} navbar>
              <Nav navbar>
                <NavItem>
                  <Link to="/login">Log In</Link>
                </NavItem>
                <NavItem>
                  <Link to="/">Sign Up</Link>
                </NavItem>
                <NavItem>
                  <Link to="/">Log Out</Link>
                </NavItem>
              </Nav>
            </Collapse>
          </Navbar>

          <Route path="/login" component={Login} exact />
          <Route path="/" component={SignUp} exact />
          <Route path="/todolist" component={TodoList} exact />
          {/* <AddTodo key={todos.priority} todos={this.state.todos}/> */}
          {/* <ItemTodo delete={this.delete} todos={this.state.todos}/> */}

        </div>
      </Router>
    );
  }
}

export default App;
