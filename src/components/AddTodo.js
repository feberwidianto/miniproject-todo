import React, { Component } from 'react'
import { CardBody, CardSubtitle, CardTitle, Form, FormGroup, Label, Col, Input, Button } from 'reactstrap';

export default class AddTodo extends Component {
    submitTodo = (e) => {
        console.log('Hello')
        e.preventDefault()
    }
    

    // getPriority = () => {
    //     return {
    //         backgroundColor: this.props.todos.priority ? 'red' : 'none'
    //     }
    // }



    render() {
        return (
            <div>
                <div className='addTodo'>
                    <CardBody>
                    <CardBody style={{textAlign: 'center'}}>
                        <CardTitle><h4>ADD TODO LIST</h4></CardTitle>
                        <CardSubtitle>Make Your Days so Beautifull</CardSubtitle>
                    </CardBody>
                    <CardBody>
                        <Form onSubmit={this.submitTodo}>
                            <FormGroup row>
                                <Label for="examplePassword" sm={2}>Activity</Label>
                                <Col sm={10}>
                                    <Input type="text" name="activity" className="activity" placeholder="What will you do, today?" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="exampleSelect" sm={2}>Duration</Label>
                                <Col sm={10}>
                                    <Input type="text" name="time" className="duration" placeholder="How much time you will do it?" />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="checkbox2" className="priority" sm={2}>Priority</Label>
                                <Col sm={{ size: 10 }}>
                                    <FormGroup check>
                                        <Label check>
                                            <Input type="checkbox" id="checkbox2" />{' '}
                                            Check me out
                                        </Label>
                                    </FormGroup>
                                </Col>
                            </FormGroup>
                            <Button color="warning" type='submit'>Submit</Button>
                        </Form>
                    </CardBody>
                    </CardBody>
                </div>
            </div>
        )
    }
}
