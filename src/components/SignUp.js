import React, { Component } from 'react'
import { Card, CardTitle, Form, FormGroup, Label, Input, Button } from 'reactstrap';


export default class SignUp extends Component {
    render() {
        return (
            <div>
                <Card body>
                <CardTitle style={{textAlign: 'center'}}><h3>SIGN UP</h3></CardTitle>
                    <Form>
                        <FormGroup>
                            <Label for="exampleEmail">Name</Label>
                            <Input type="text" name="name" id="exampleEmail" placeholder="Full Name" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="exampleEmail">Email</Label>
                            <Input type="email" name="email" id="exampleEmail" placeholder="Email Address" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="examplePassword">Password</Label>
                            <Input type="password" name="password" id="examplePassword" placeholder="Your Password" />
                        </FormGroup>
                    </Form>
                    <Button color="primary">GET STARTED</Button>
                </Card>
            </div>
        )
    }
}
