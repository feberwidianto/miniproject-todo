import React, { Component } from 'react'
import { Card, CardBody, CardText, CardTitle, CardSubtitle, Button } from 'reactstrap';

export default class ItemTodo extends Component {

    


    render() {

        const items = this.props.todo.map(item => {
            return (
                <div>
                    <Card>
                        <CardBody>
                            <CardText><h4>{item.activity}</h4></CardText>
                            <CardText><p>{item.duration}</p></CardText>
                            <Button color="danger" className="delete"> delete </Button> {' '} 
                            <Button color="success" className='check'> checklist </Button>
                        </CardBody>
                    </Card>
                </div>
            )
        })
        return (
            <div>
                <Card>
                    <CardBody>
                        <CardBody style={{ textAlign: 'center' }}>
                            <CardTitle><h3>My Agenda</h3></CardTitle>
                            <CardSubtitle>Beautifull Days!</CardSubtitle>
                        </CardBody>
                        <hr></hr>
                    </CardBody>
                    <CardBody>
                        {items}
                    </CardBody>
                </Card>
            </div>
        )
    }
}
