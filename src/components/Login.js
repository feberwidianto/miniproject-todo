import React, { Component } from 'react'
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import { Card, CardTitle, Form, FormGroup, Label, Input, Button } from 'reactstrap';



export default class Login extends Component {
  render() {
    return (
      <div>
        <Card body>
        <CardTitle style={{textAlign: 'center', color:'black'}}><h3>LOG IN</h3></CardTitle>
          <Form>
            <FormGroup>
              <Label for="exampleEmail">Email</Label>
              <Input type="email" name="email" id="exampleEmail" placeholder="Email Address" />
            </FormGroup>
            <FormGroup>
              <Label for="examplePassword">Password</Label>
              <Input type="password" name="password" id="examplePassword" placeholder="Your Password" />
            </FormGroup>
            <a href="#" style={{float:'right'}}>Forget your password?</a>
          </Form>
          <Button color="primary"><Link to="/todolist" style={{color:'white'}}>LOG IN</Link></Button>
        </Card>

      </div>
    )
  }
}
